
package Modelo;

public class Calculo {
    int capital;
    int interes;
    int numero;

    public Calculo() {
    }

   
    
    /*
    I = C * (i/100) * n; donde I = interés simple producido, C = capital, i = tasa interés anual y n = número de años 

Ejemplo: Un capital de $100.000 a una tasa anual del 10% en 3 años genera un interés simple de: 

I = $100.000 * 0.1 * 3 = $30.000 

 
    */
 
    public float getResultado(int c, int i, int n){
        float resp;
        float interes = (float)i/100; 
     
        resp = (c * interes ) * n;
        
        return resp;
    }
    
    public int getCapital() {
        return capital;
    }

    public int getInteres() {
        return interes;
    }

    public int getNumero() {
        return numero;
    }

    public void setCapital(int capital) {
        this.capital = capital;
    }

    public void setInteres(int interes) {
        this.interes = interes;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    
    
    
}
